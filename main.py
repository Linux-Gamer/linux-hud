import pygame
import time
import pygetwindow as gw
import win32api
import win32con
import win32gui
import win32ui
import win32process
import win32com.client
import pynvml

# Initialize Pygame
pygame.init()

# Set up the screen dimensions
screen_width = 800
screen_height = 600

# Create an overlay window
hwnd = win32gui.FindWindow(None, "Game Window")  # Replace "Game Window" with the title of your game
game_rect = win32gui.GetWindowRect(hwnd)
overlay_width = game_rect[2] - game_rect[0]
overlay_height = game_rect[3] - game_rect[1]
overlay_hwnd = win32gui.CreateWindowEx(
    win32con.WS_EX_TOPMOST | win32con.WS_EX_LAYERED | win32con.WS_EX_TRANSPARENT,
    win32gui.RegisterClass(None),
    "Overlay",
    win32con.WS_POPUP,
    game_rect[0],
    game_rect[1],
    overlay_width,
    overlay_height,
    None,
    None,
    None,
    None
)
win32gui.SetLayeredWindowAttributes(overlay_hwnd, win32api.RGB(0, 0, 0), 255, win32con.LWA_COLORKEY)

# Define colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Define font
font = pygame.font.Font(None, 36)

# Initialize NVML
pynvml.nvmlInit()
device_handle = pynvml.nvmlDeviceGetHandleByIndex(0)  # Use the appropriate GPU index

# Function to capture screen
def capture_screen():
    hwin = win32gui.GetDesktopWindow()
    width = overlay_width
    height = overlay_height
    left = 0
    top = 0
    
    hwndDC = win32gui.GetWindowDC(hwin)
    mfcDC = win32ui.CreateDCFromHandle(hwndDC)
    saveDC = mfcDC.CreateCompatibleDC()

    saveBitMap = win32ui.CreateBitmap()
    saveBitMap.CreateCompatibleBitmap(mfcDC, width, height)

    saveDC.SelectObject(saveBitMap)

    result = windll.user32.PrintWindow(hwin, saveDC.GetSafeHdc(), 0)
    bmpinfo = saveBitMap.GetInfo()
    bmpstr = saveBitMap.GetBitmapBits(True)

    image = np.fromstring(bmpstr, dtype='uint8')
    image = image.reshape((height, width, 4))

    mfcDC.DeleteDC()
    saveDC.DeleteDC()
    win32gui.ReleaseDC(hwin, hwndDC)
    win32gui.DeleteObject(saveBitMap.GetHandle())
    
    return image

# Function to update the overlay
def update_overlay():
    # Capture the game screen
    game_screen = capture_screen()

    # Create a Pygame surface from the captured screen
    game_surface = pygame.surfarray.make_surface(game_screen)
    
    # Clear the overlay screen
    overlay_screen = pygame.Surface((overlay_width, overlay_height), pygame.SRCALPHA)
    overlay_screen.fill((0, 0, 0, 0))
    
    # Render and display FPS on the overlay screen
    fps_text = font.render("FPS: " + str(fps), True, WHITE)
    overlay_screen.blit(fps_text, (10, 10))
    
    # Combine the game screen and overlay screen
    combined_screen = pygame.Surface((overlay_width, overlay_height), pygame.SRCALPHA)
    combined_screen.blit(game_surface, (0, 0))
    combined_screen.blit(overlay_screen, (0, 0))
    
    # Update the overlay window
    hdc = win32gui.GetDC(overlay_hwnd)
    hsurf = pygame.surfarray.pixels3d(combined_screen)
    win32gui.SetWindowPos(overlay_hwnd, win32con.HWND_TOPMOST, game_rect[0], game_rect[1], overlay_width, overlay_height, 0)
    win32gui.UpdateLayeredWindow(overlay_hwnd, hdc, (game_rect[0], game_rect[1]), (overlay_width, overlay_height), hsurf, (0, 0, 0, 255), win32con.LWA_COLORKEY)
    win32gui.ReleaseDC(overlay_hwnd, hdc)

# Main game loop
running = True
while running:
    # Event handling
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    # Calculate FPS
    start_time = time.time()
    
    # Game logic and rendering here
    
    end_time = time.time()
    fps = int(1 / (end_time - start_time))
    
    # Update the overlay
    update_overlay()

# Quit the game and clean up
pygame.quit()
win32gui.DestroyWindow(overlay_hwnd)
